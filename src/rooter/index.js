import { createRouter, createWebHistory } from "vue-router";
import Home from '@/views/Home.vue';
import Formulaire from '@/views/Formulaire.vue';
import Detail from '@/views/Detail.vue'
import Confirmation from '@/views/Confirmation.vue'
import Thanks from '@/views/Thanks.vue'


const routes = [
    {
        name: 'Home',
        path: '/',
        component: Home
    }, 
    {
        name: 'Formulaire',
        path: '/formulaire',
        component: Formulaire
    },
    {
        name: 'Detail',
        path: '/detail/:id',
        component: Detail,
        props:true
    },
    {
        name: 'Confirmation',
        path:'/confirmation',
        component: Confirmation
    },
    {
        name: 'Thanks',
        path:'/thanks',
        component: Thanks
    },
]
const router = createRouter({
    history: createWebHistory(),
    routes,
})
export default router;